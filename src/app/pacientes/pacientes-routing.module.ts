import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TemplateComponent } from 'app/shared-template/template/template.component';
import { CrearComponent } from './crear/crear.component';
import { IndexComponent } from './index/index.component';

const routes: Routes = [
  {
    path: '', component: TemplateComponent,
    children: [
        {path: '', component: IndexComponent, outlet: 'content'}
    ]
},
{
    path: 'crear', component: TemplateComponent,
    children: [
        {path: '', component: CrearComponent, outlet: 'content'}
    ]
},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PacientesRoutingModule { }
