import { Component, Injector, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ComunaRestService, GeneroRestService, GuardarPacienteDto, PacienteRestService, Paciente } from 'app/core/clients-api-modules/paciente-service';
import { ComunaAux } from 'app/core/model-aux/comuna.aux';
import { GeneroAux } from 'app/core/model-aux/genero.aux';
import * as _ from 'lodash';
import { ConfirmationService, Message, SelectItem } from 'primeng/api';
import { isNumeric } from 'rxjs/util/isNumeric';
import { PersonaDto } from '../../core/dto/persona.dto';
import { UsuarioDto } from '../../core/dto/usuario.dto';
import { ConstantesUtils } from '../../core/utils/constantes.utils';
import { LocaleUtils } from '../../core/utils/locale.utils';
import { CustomsValidators } from '../../core/validators/customs.validators';
import { FlashScopeService } from 'app/core/services/flash-scope.service';
import { RutConverter } from 'app/core/converters/rut.converter';
import { BlockuiService } from 'app/core/services/blockui.service';

@Component({
    selector: 'app-crear',
    templateUrl: './crear.component.html',
    styleUrls: ['./crear.component.css']
})
export class CrearComponent implements OnInit {

    idPaciente: number;
    pacienteEntrada: Paciente;

    msgs: Message[];
    modoEditar: boolean;
    usuarioEntrada: UsuarioDto;
    formPaciente: FormGroup;
    submitted: boolean;
    exitoDialogoVisible: boolean;
    perfilesSelectItems: SelectItem[];
    comunasSelectItems: SelectItem[];
    generoSelectItems: SelectItem[];

    /* calendar */
    minDate: Date;
    maxDate: Date;
    yearRange: string;
    calLocale: any;
    es: any;

    private fb: FormBuilder;
    private router: Router;
    private confirmationService: ConfirmationService;
    private pacienteServiceApi: PacienteRestService;
    private comunaRestApi: ComunaRestService;
    private generoRestApi: GeneroRestService;
    private flashScope: FlashScopeService;
    private blockUI: BlockuiService;

    constructor(private injector: Injector) {
        this.fb = this.injector.get(FormBuilder)
        this.router = this.injector.get(Router);
        this.confirmationService = this.injector.get(ConfirmationService);
        this.pacienteServiceApi = this.injector.get(PacienteRestService);
        this.comunaRestApi = this.injector.get(ComunaRestService);
        this.generoRestApi = this.injector.get(GeneroRestService);
        this.flashScope = this.injector.get(FlashScopeService);
        this.blockUI = this.injector.get(BlockuiService);

    }

    ngOnInit() {
        console.log('iniciando modulo edicion-creacion usuario');
        const idPacienteParam: number = this.flashScope.get(ConstantesUtils.PARAM_ID_PACIENTE);
        console.log(`idPacienteParam; ${idPacienteParam}`)
        if (idPacienteParam) {
            this.pacienteServiceApi.traerPacientePorIdUsingGET(idPacienteParam)
                .subscribe(data => {
                    if (data) {
                        console.log(`data IF: ${JSON.stringify(data)}`)
                        this.iniciarModoEditar(data);
                    } else {
                        this.iniciarModoCrear();
                    }
                });
        } else {
            this.iniciarModoCrear();
        }
        this.es = LocaleUtils.getCalendarEs()
        this.llenarGenero();
        this.llenarComunas();
    }

    private cargarPaciente() {
        this.pacienteServiceApi.traerPacientePorIdUsingGET(this.idPaciente)
            .subscribe(paciente => {
                this.pacienteEntrada = paciente;
            });
    }

    private llenarGenero() {
        this.blockUI.block(1);
        this.generoRestApi.traerTodosUsingGET()
            .subscribe(genero => {
                this.generoSelectItems = _.concat([ConstantesUtils.createSeleccionarSelectItem()],
                    _.map(genero, GeneroAux.toSelectItem)
                )
                this.blockUI.tick();
            });
    }

    private llenarComunas() {
        this.blockUI.block(1);
        this.comunaRestApi.traerComunaPorIdComunaUsingGET(1)
            .subscribe(comuna => {
                this.comunasSelectItems = _.concat([ConstantesUtils.createSeleccionarSelectItem()],
                    _.map(comuna, ComunaAux.toSelectItem)
                )
                this.blockUI.tick();
            });
    }

    confirmAceptar(): void {
        console.log('entre confirmAceptar');
        this.submitted = true;
        if (!this.formPaciente.valid) {
            return;
        }
        this.confirmationService.confirm({
            message: 'Está seguro que desea guardar el usuario?',
            accept: () => {
                console.log('acepta guardar!');
                this.guardar();
                this.volver();
            }
        });
    }


    volver(): void {
        this.router.navigate([ConstantesUtils.ROUTE_GESTIONAR_PACIENTES]);
    }

    /* privados */

    private iniciarModoCrear(): void {
        console.log(`Modo Crear`)
        this.modoEditar = false;
        this.pacienteEntrada = <Paciente>{};
        this.crearFormulario();
    }

    private iniciarModoEditar(paciente: Paciente): void {
        console.log(`Modo Editar: ${paciente.idPaciente}`)
        this.pacienteEntrada = paciente;
        this.modoEditar = true;
        this.crearFormulario();
    }

    private crearFormulario(): void {
        const dto: Paciente = this.pacienteEntrada;
        console.log(`fecha nacimeinto: ${dto.fechaNacimiento}`);

        this.formPaciente = this.fb.group({
            id: [dto === null ? dto.idPaciente: null],
            eliminado: [null],
            run: [dto === null? null: RutConverter.toString(dto.run) , CustomsValidators.validateRUN],
            nombres: [dto.nombres],
            fechaNacimiento: [dto === null ? null : (dto.fechaNacimiento)],
            genero: [dto.genero],
            direccion: [dto.direccion],
            comuna: [dto.comuna],
            telefono: [dto.telefono],
            email: [dto.email],
            departamento: [dto.departamento]

        });
    }

    private initCalendar(): void {
        this.maxDate = new Date();
        this.minDate = new Date();
        this.maxDate.setFullYear(this.maxDate.getFullYear() - 18);
        this.minDate.setFullYear(this.minDate.getFullYear() - 90);
        this.yearRange = this.minDate.getFullYear() + ':' + this.maxDate.getFullYear();
        this.calLocale = LocaleUtils.getCalendarEs();
    }

    private guardar(): void {
        console.log('entre guardar');
        this.blockUI.block(1);
        const pacienteDto: GuardarPacienteDto = this.formToDto();
        this.pacienteServiceApi.guardarPacienteUsingPUT(pacienteDto)
            .subscribe(() => {
                console.log('éxito guardar!');
                this.exitoDialogoVisible = true;
                this.blockUI.tick();
            });
    }

    private formToDto(): GuardarPacienteDto {
        const pacienteDto: GuardarPacienteDto = <GuardarPacienteDto>{};
        pacienteDto.idPaciente = this.formPaciente.get('id').value;
        pacienteDto.run = this.formPaciente.get('run').value ? RutConverter.toNumber(this.formPaciente.get('run').value): null;
        pacienteDto.nombres = this.formPaciente.get('nombres').value;
        pacienteDto.fechaNacimiento = this.formPaciente.get('fechaNacimiento').value;
        pacienteDto.genero = this.formPaciente.get('genero').value;
        pacienteDto.direccion = this.formPaciente.get('direccion').value;
        pacienteDto.comuna = this.formPaciente.get('comuna').value;
        pacienteDto.telefono = this.formPaciente.get('telefono').value;
        pacienteDto.email = this.formPaciente.get('email').value;
        pacienteDto.departamento = this.formPaciente.get('departamento').value;
        
        return pacienteDto;
    }

    // private emailUnicoValidator(): AsyncValidatorFn {
    //     return (control: AbstractControl): Promise<any> => {
    //         clearTimeout(this.emailTimeOut);
    //         return new Promise<any>(resolve => {
    //             this.emailTimeOut = setTimeout(() => {
    //                 this.usuarioService.traerPorEmail(control.value, false)
    //                     .subscribe(user => {
    //                         if (user && user.idPersona !== this.usuarioEntrada.idPersona) {
    //                             resolve({ emailUnico: true });
    //                         } else {
    //                             resolve(null);
    //                         }
    //                     }, error => {
    //                         resolve({ emailUnico: true });
    //                     });
    //             }, 1000);
    //         });
    //     };
    // }

    // private runUnicoValidator(): AsyncValidatorFn {
    //     return (control: AbstractControl): Promise<any> => {
    //         clearTimeout(this.runTimeOut);
    //         return new Promise<any>(resolve => {
    //             this.runTimeOut = setTimeout(() => {
    //                 this.usuarioService.traerPorRun(RutConverter.toNumber(control.value), false)
    //                     .subscribe(user => {
    //                         if (user && user.idPersona !== this.usuarioEntrada.idPersona) {
    //                             resolve({ runUnico: true });
    //                         } else {
    //                             resolve(null);
    //                         }
    //                     }, error => {
    //                         resolve({ runUnico: true });
    //                     });
    //             }, 1000);
    //         });
    //     };
    // }

}

