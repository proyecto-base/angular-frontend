import { Component, Injector, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PacienteRestService } from 'app/core/clients-api-modules/paciente-service/api/api';
import { Paciente } from 'app/core/clients-api-modules/paciente-service/model/models';
import { ConstantesUtils } from 'app/core/utils/constantes.utils';
import { FlashScopeService } from 'app/core/services/flash-scope.service';
import { BlockuiService } from 'app/core/services/blockui.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  colsTablaPacientes: any[] = [];
  valuesTablaPacientes: any[] = [];

  private pacienteServiceApi: PacienteRestService;
  private router: Router;
  private flashScope: FlashScopeService;
  private blockUI: BlockuiService;

  constructor(private injector: Injector) {
    this.pacienteServiceApi = this.injector.get(PacienteRestService);
    this.router = this.injector.get(Router);
    this.flashScope = this.injector.get(FlashScopeService);
    this.blockUI = this.injector.get(BlockuiService);
  }

  ngOnInit() {
    this.buscar();
    this.crearColumnasTablaPacientes();
  }

  public irNuevoPaciente() {
    this.router.navigate([ConstantesUtils.ROUTE_CREAR_PACIENTES]);
  }

  public editarPaciente(paciente: Paciente) {
    console.log(`Editar Paciente: ${JSON.stringify(paciente.idPaciente)}`)
    this.flashScope.add(ConstantesUtils.PARAM_ID_PACIENTE, paciente.idPaciente);
    this.router.navigate([ConstantesUtils.ROUTE_CREAR_PACIENTES]);
   }

  public eliminarPaciente(paciente: Paciente) { 
    console.log(`Eliminar Paciente: ${JSON.stringify(paciente)}`)
  }

  public nuevaConsulta() { }

  private buscar() { 
    this.blockUI.block(1);
    this.pacienteServiceApi.traerTodosUsingGET1()
    .subscribe(pacientes => {
      this.valuesTablaPacientes = this.pacientesToTablaPacientes(pacientes);
      this.blockUI.tick();
    });
  }

  private crearColumnasTablaPacientes() {
    this.colsTablaPacientes = [
      { field: 'id', header: '#', style: {} },
      { field: 'nombre', header: 'Nombre', style: {} },
      { field: 'rut', header: 'RUT', style: {} },
      { field: 'ultimaAtención', header: 'Ultima Atención', style: {} },
      { field: 'acciones', header: 'Acciones', style: {} }
    ]
  }

  private pacientesToTablaPacientes(pacientes: Paciente[]): any[] {
    const tableContent: any[] = [];
    pacientes.forEach(paciente => {
      const fila: any = {};
      fila.id = paciente.idPaciente;
      fila.nombre = paciente.nombres;
      fila.rut = paciente.run;
      fila.ultimaAtencion = null;
      fila.acciones = null;
      fila.paciente = paciente;
      tableContent.push(fila);
    });
    return tableContent;
  }

}
