import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedTemplateModule } from 'app/shared-template/shared-template.module';
import { TableModule } from 'primeng/table';
import { CrearComponent } from './crear/crear.component';
import { IndexComponent } from './index/index.component';
import { PacientesRoutingModule } from './pacientes-routing.module';
import { SharedCommonsModule } from '../shared-commons/shared-commons.module';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TooltipModule } from 'primeng/tooltip';
import { CalendarModule } from 'primeng/calendar';
import { GrowlModule } from 'primeng/components/growl/growl';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';

@NgModule({
  declarations: [CrearComponent, IndexComponent],
  imports: [
    CommonModule,
    PacientesRoutingModule,
    SharedCommonsModule,
    SharedTemplateModule,
    TableModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    TooltipModule,
    CalendarModule,
    GrowlModule,
    ConfirmDialogModule,
    DialogModule,
    ButtonModule,
    DropdownModule,
    InputTextModule

  ]
})
export class PacientesModule { }
