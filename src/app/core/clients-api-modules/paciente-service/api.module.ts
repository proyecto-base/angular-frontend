import { NgModule, ModuleWithProviders, SkipSelf, Optional } from '@angular/core';
import { Configuration } from './configuration';
import { HttpClient } from '@angular/common/http';


import { BasicErrorControllerService } from './api/basicErrorController.service';
import { ComunaRestService } from './api/comunaRest.service';
import { GeneroRestService } from './api/generoRest.service';
import { HelloworldRestService } from './api/helloworldRest.service';
import { PacienteRestService } from './api/pacienteRest.service';
import { PerfilRestService } from './api/perfilRest.service';
import { ProvinciaRestService } from './api/provinciaRest.service';
import { RegionRestService } from './api/regionRest.service';
import { SecurityRestService } from './api/securityRest.service';
import { ShowcaseRestService } from './api/showcaseRest.service';
import { UsuarioRestService } from './api/usuarioRest.service';
import { VersionRestService } from './api/versionRest.service';

@NgModule({
  imports:      [],
  declarations: [],
  exports:      [],
  providers: [
    BasicErrorControllerService,
    ComunaRestService,
    GeneroRestService,
    HelloworldRestService,
    PacienteRestService,
    PerfilRestService,
    ProvinciaRestService,
    RegionRestService,
    SecurityRestService,
    ShowcaseRestService,
    UsuarioRestService,
    VersionRestService ]
})
export class ApiModule {
    public static forRoot(configurationFactory: () => Configuration): ModuleWithProviders {
        return {
            ngModule: ApiModule,
            providers: [ { provide: Configuration, useFactory: configurationFactory } ]
        };
    }

    constructor( @Optional() @SkipSelf() parentModule: ApiModule,
                 @Optional() http: HttpClient) {
        if (parentModule) {
            throw new Error('ApiModule is already loaded. Import in your base AppModule only.');
        }
        if (!http) {
            throw new Error('You need to import the HttpClientModule in your AppModule! \n' +
            'See also https://github.com/angular/angular/issues/20575');
        }
    }
}
