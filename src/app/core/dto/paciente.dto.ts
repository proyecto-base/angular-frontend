import { ComunaDto } from "./comuna.dto";
import { GeneroDto } from "./genero.dto";

export class PacienteDto {
	idPaciente: number;
	run: number;
	nombres:string;
	fechaNacimiento: Date;
	genero: GeneroDto;
	direccion: string;
	comuna: ComunaDto;
	telefono: string;
	email: string;
	departamento: string;
}