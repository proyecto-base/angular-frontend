import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { PacienteDto } from '../dto/paciente.dto';
import { HttpService } from './http.service';

@Injectable({
	providedIn: 'root'
})
export class PacienteService {

	constructor(private httpService: HttpService) { }

	traerPorId(idPaciente: number, blockui: boolean = true): Observable<PacienteDto> {
		const urlGet = '/pacientes/id/' + idPaciente;
		return this.httpService.getJson<PacienteDto>(urlGet, blockui);
	}

	traerTodos(blockui: boolean = true): Observable<PacienteDto> {
		const urlGet = '/pacientes/traerTodos/';
		return this.httpService.getJson<PacienteDto>(urlGet, blockui);
	}

	guardar(pacienteDto: PacienteDto, blockui: boolean = true): Observable<any> {
		return this.httpService.postJson<any>('/pacientes/guardar', JSON.stringify(pacienteDto), blockui);
	}

}
