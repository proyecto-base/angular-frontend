import { CommonModule, HashLocationStrategy, LocationStrategy } from '@angular/common';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgIdleModule } from '@ng-idle/core';
/* Translate */
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { NgxWebstorageModule } from 'ngx-webstorage';
import { ConfirmationService } from 'primeng/api';
import { ButtonModule } from 'primeng/button';
/* primeng */
import { DialogModule } from 'primeng/dialog';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { BlockuiComponent } from './blockui/blockui.component';
/** apis */
import { ApiModule as PacienteServiceApiModule,  BASE_PATH as PacienteServiceBasePath } from './clients-api-modules/paciente-service';
import { GlobalMessageComponent } from './global-message/global-message.component';
import { IdleDetectorComponent } from './idle-detector/idle-detector.component';
import { ApiUrlInterceptor } from './interceptors/api-url.interceptor';
import { AuthInterceptor } from './interceptors/auth.interceptor';
import { BlockuiInterceptor } from './interceptors/blockui.interceptor';
import { ErrorInterceptor } from './interceptors/error.interceptor';
import { ScrollTopComponent } from './scroll-top/scroll-top.component';
import { ShellComponent } from './shell/shell.component';
import { ENVIRONMENT } from 'environments/environment';

/* AoT requires an exported function for factories */
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    BlockuiComponent,
    ShellComponent,
    GlobalMessageComponent,
    IdleDetectorComponent,
    ScrollTopComponent,
  ],
  imports: [
    /* primeng */
    DialogModule,
    ButtonModule,
    ScrollPanelModule,
    /** apis */
    PacienteServiceApiModule,
    /* otros */
    CommonModule,
    HttpClientModule,
    NgIdleModule.forRoot(),
    NgxWebstorageModule.forRoot(),
    RouterModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
  })
  ],
  providers: [
    HttpClient,
    ConfirmationService,
  // HTTP INTERCEPTORS
  {
    provide: HTTP_INTERCEPTORS,
    useClass: ApiUrlInterceptor,
    multi: true
},
{
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
},
{
    provide: HTTP_INTERCEPTORS,
    useClass: BlockuiInterceptor,
    multi: true
},
{
    provide: HTTP_INTERCEPTORS,
    useClass: ErrorInterceptor,
    multi: true
},
// Api Base Path
{
  provide: PacienteServiceBasePath,
  useValue: `${ENVIRONMENT.API_URL}`,
  multi: true
},
// Others
{
    provide: LocationStrategy,
    useClass: HashLocationStrategy
}],
  exports: [
    ShellComponent]
})
export class CoreModule { }
