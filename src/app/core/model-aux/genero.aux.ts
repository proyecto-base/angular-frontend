import { Genero } from "../clients-api-modules/paciente-service";
import { SelectItem } from "primeng/api";

export class GeneroAux {

    public static readonly GENERO_FEMENINO = 1;
    public static readonly GENERO_MASCULINO = 2;

    public static toSelectItem(genero: Genero): SelectItem {
        return <SelectItem>{ label: genero.nombre, value: genero }
    }

} 