import { SelectItem } from "primeng/api";
import { Comuna } from "../clients-api-modules/paciente-service/";

export class ComunaAux {

    public static toSelectItem(comuna: Comuna): SelectItem {
        return <SelectItem>{ label: comuna.nombre, value: comuna }
    }

}