import {Subject} from 'rxjs/Subject';

export class BeginLoadingEventEmmiter extends Subject<string> {

    constructor() {
        super();
    }

    emit(value) {
        super.next(value);
    }
}

export class EndLoadingEventEmmiter extends Subject<string> {

    constructor() {
        super();
    }

    emit(value) {
        super.next(value);
    }
}
